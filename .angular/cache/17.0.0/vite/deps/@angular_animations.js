import {
  AUTO_STYLE,
  AnimationBuilder,
  AnimationFactory,
  AnimationGroupPlayer,
  BrowserAnimationBuilder,
  NoopAnimationPlayer,
  animate,
  animateChild,
  animation,
  group,
  keyframes,
  query,
  sequence,
  stagger,
  state,
  style,
  transition,
  trigger,
  useAnimation,
  ɵPRE_STYLE
} from "./chunk-HKYTZXAY.js";
import "./chunk-UGFR2TEF.js";
import "./chunk-C2O4IEUZ.js";
import "./chunk-AFRS2OIU.js";
import "./chunk-OXCW2X5T.js";
export {
  AUTO_STYLE,
  AnimationBuilder,
  AnimationFactory,
  NoopAnimationPlayer,
  animate,
  animateChild,
  animation,
  group,
  keyframes,
  query,
  sequence,
  stagger,
  state,
  style,
  transition,
  trigger,
  useAnimation,
  AnimationGroupPlayer as ɵAnimationGroupPlayer,
  BrowserAnimationBuilder as ɵBrowserAnimationBuilder,
  ɵPRE_STYLE
};
//# sourceMappingURL=@angular_animations.js.map
