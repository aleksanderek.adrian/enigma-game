const { app, BrowserWindow, ipcMain } = require("electron");

let mainWindow;

const createMainWindow = () => {
  mainWindow = new BrowserWindow({
    icon: "src/favicon.ico",
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  mainWindow.removeMenu();
  mainWindow.on("closed", () => app.quit());
  mainWindow.setFullScreen(true);
  mainWindow.loadFile("dist/enigma/browser/index.html");
}

app.whenReady().then(createMainWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});

ipcMain.on("app:exit", () => {
  app.quit();
});
