# Enigma Video Game

![Logo](logo.png)

A game that involves matching dots to a pattern. Created in Angular and Electron.

![Preview image](preview.png)

## Technologies

- TypeScript 5.2.2
- Angular.js 17.0.0
- Electron.js 27.0.4

## Setup

Installation using npm.

`npm install`

To start application in dev mode using electron:

`npm start`

Dev in browser:

`npm run dev:browser`

To build application:

`npm run electron:build`

App will be built in dist folder.
